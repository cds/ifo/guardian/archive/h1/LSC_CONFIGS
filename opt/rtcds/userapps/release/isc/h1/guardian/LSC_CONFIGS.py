# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: $
# $HeadURL: $

from guardian import GuardState, GuardStateDecorator
from guardian.ligopath import userapps_path
from guardian import NodeManager
from subprocess import check_output
import cdsutils
from gpstime import gpstime
tconvert = gpstime.tconvert
import time
import sys

##################################################

#ca_monitor = True
ca_monitor = False
ca_monitor_notify = False

##################################################

from isclib import pdstep
from isclib.burt import iscBurt
import lscparams as lscparams
from ISC_GEN_STATES import *
from ISC_library import *

##################################################

#TODO: DRMI lock threshold input power scaling
#TODO: Power scaling for other thresholds
#TODO: DC readout transition
#TODO: Turn on WFS once at zero carm offset
#TODO: Find carrier resonance in OMC length

# scripts that we shouldn't be using
#drmi_to_1f_script_file = userapps_path('lsc', IFO.lower(), 'scripts', 'autolock', 'drfpmi', 'drmi_to_1f.sh')
#aligner_script_file = userapps_path('sus', 'common', 'scripts', 'aligner.py')


##################################################
## name the filters
##################################################
def prcl_filt():
    return ezca.get_LIGOFilter('LSC-PRCL')

def mich_filt():
    return ezca.get_LIGOFilter('LSC-MICH')

def srcl_filt():
    return ezca.get_LIGOFilter('LSC-SRCL')

def arm_filt(arm):
    return ezca.get_LIGOFilter('LSC-%sARM'%arm)

def prm_m2_filt():
    return ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L')

def srm_m2_filt():
    return ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L')

def asc_dc3_yaw_filter():
    return ezca.get_LIGOFilter('ASC-DC3_Y')

def asc_dc3_pitch_filter():
    return ezca.get_LIGOFilter('ASC-DC3_P')

def asc_dc4_yaw_filter():
    return ezca.get_LIGOFilter('ASC-DC4_Y')

def asc_dc4_pitch_filter():
    return ezca.get_LIGOFilter('ASC-DC4_P')

def asc_prm_yaw_filter():
    return ezca.get_LIGOFilter('ASC-PRC1_Y')

def asc_prm_pitch_filter():
    return ezca.get_LIGOFilter('ASC-PRC1_P')

def asc_pr3_yaw_filter():
    return ezca.get_LIGOFilter('ASC-PRC2_Y')

def asc_pr3_pitch_filter():
    return ezca.get_LIGOFilter('ASC-PRC2_P')

def asc_mich_yaw_filter():
    return ezca.get_LIGOFilter('ASC-MICH_Y')

def asc_mich_pitch_filter():
    return ezca.get_LIGOFilter('ASC-MICH_P')

def asc_sr3_yaw_filter():
    return ezca.get_LIGOFilter('ASC-SRC1_Y')
def asc_sr3_pitch_filter():
    return ezca.get_LIGOFilter('ASC-SRC1_P')
def asc_sr2_yaw_filter():
    return ezca.get_LIGOFilter('ASC-SRC2_Y')
def asc_sr2_pitch_filter():
    return ezca.get_LIGOFilter('ASC-SRC2_P')

def asc_im4_yaw_filter():
    return ezca.get_LIGOFilter('ASC-INP1_Y')
def asc_im4_pitch_filter():
    return ezca.get_LIGOFilter('ASC-INP1_P')

# BS top stage LOCK_P and _Y
def sus_bs_m1_pit_filter():
	return ezca.get_LIGOFilter('SUS-BS_M1_LOCK_P')
def sus_bs_m1_yaw_filter():
	return ezca.get_LIGOFilter('SUS-BS_M1_LOCK_Y')
# PRM top stage LOCK_P and _Y
def sus_prm_m1_pit_filter():
	return ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_P')
def sus_prm_m1_yaw_filter():
	return ezca.get_LIGOFilter('SUS-PRM_M1_LOCK_Y')
# PR3 top stage LOCK_P and _Y
def sus_pr3_m1_pit_filter():
	return ezca.get_LIGOFilter('SUS-PR3_M1_LOCK_P')
def sus_pr3_m1_yaw_filter():
	return ezca.get_LIGOFilter('SUS-PR3_M1_LOCK_Y')
# SR3 top stage LOCK_P and _Y
def sus_sr3_m1_pit_filter():
	return ezca.get_LIGOFilter('SUS-SR3_M1_LOCK_P')
def sus_sr3_m1_yaw_filter():
	return ezca.get_LIGOFilter('SUS-SR3_M1_LOCK_Y')

##################################################
# NODES
##################################################

nodes = NodeManager([
         'SUS_ITMY',
         'SUS_ITMX',
         'SUS_PRM',
         'SUS_SRM',
         'SUS_ETMY', 
         'SUS_ETMX'
         ])

##################################################
# STATES
##################################################

# FIXME: this needs to check state and move to the right place on the
# graph
class INIT(GuardState):
    request = True
    def main(self):
         log("initializing subordinate nodes...")
         nodes.set_managed()
    def run(self):
         return True

# reset everything to the good values for acquistion.  These values
# are stored in the down script.
class DOWN(GuardState):
    goto = True
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        ezca['IMC-MCL_GAIN'] = 1
        prcl_filt().switch_off('FM2', 'FM3','INPUT')
        mich_filt().switch_off('FM2', 'FM3','INPUT')
        srcl_filt().switch_off('FM2', 'FM3','INPUT')
        ezca['LSC-PRCL_GAIN'] = 0
        ezca['LSC-MICH_GAIN'] = 0
        ezca['LSC-SRCL_GAIN'] = 0
        ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').switch_off('INPUT')
        ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').switch_off('INPUT')
        ezca['SUS-SRM_M2_LOCK_L_GAIN'] = 0
        ezca['SUS-PRM_M2_LOCK_L_GAIN'] = 0
        ezca['SUS-PRM_M2_LOCK_L_RSET'] = 2
        ezca['SUS-SRM_M2_LOCK_L_RSET'] = 2  
        #turn of ASC
        ascList = ['INP1', 'INP2', 'PRC1', 'PRC2', 'SRC1', 'SRC2', 'MICH', 'DC1', 'DC2']
        ascList = ['ASC-{0}_'.format(ii) for ii in ascList]
        ascList = [ii+'P' for ii in ascList] + [ii+'Y' for ii in ascList]
        for ascLoop in ascList:
            ezca.get_LIGOFilter(ascLoop).switch_off('INPUT')
            ezca[ascLoop+'_RSET'] = 2
        # removed ASC PRMI PRM, BS stuff
        # set up PRM feedback to M2
        ezca.switch('SUS-PRM_M1_LOCK_P','OUTPUT','ON') 
        ezca.switch('SUS-PRM_M1_LOCK_Y','OUTPUT','ON') 
        ezca['SUS-PRM_M2_LOCK_OUTSW_P'] = 0 
        ezca['SUS-PRM_M2_LOCK_OUTSW_Y'] = 0 

        # set up BS feedback to M2
        ezca.switch('SUS-BS_M1_LOCK_P','OUTPUT','ON') 
        ezca.switch('SUS-BS_M1_LOCK_Y','OUTPUT','ON') 
        ezca['SUS-BS_M2_LOCK_OUTSW_P'] = 0 
        ezca['SUS-BS_M2_LOCK_OUTSW_Y'] = 0 
        self.timer['pause'] = 2
        #this was in the run, but was intering with arm locking when this is down
        arm_filt('X').ramp_gain(0, 10)
        arm_filt('Y').ramp_gain(0, 10)
        #turn off ASC
        ezca.get_LIGOFilter('ASC-INP1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-MICH_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-MICH_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')

        # clear histories
        ezca['SUS-PRM_M1_LOCK_P_RSET'] = 2
        ezca['SUS-PRM_M1_LOCK_Y_RSET'] = 2
        ezca['ASC-PRC1_P_RSET'] = 2
        ezca['ASC-PRC1_Y_RSET'] = 2

    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if MC_locked():
           return True
        else:
           notify('MC is not locked')

class ALIGN_SUS_FOR_ALS(GuardState):
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        for nodename in ['SUS_ETMY', 'SUS_ETMX', 'SUS_ITMY', 'SUS_ITMX']:
            if not nodes[nodename].NOTIFICATION:
                nodes[nodename] = 'ALIGNED'
                notify('NOt Aligning %s because of notification'%nodename)
        nodes['SUS_PRM'] = 'MISALIGNED'
        nodes['SUS_SRM'] = 'MISALIGNED'
        self.timer['wait_for_sus_settle'] = 10
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if self.timer['wait_for_sus_settle']:
            return True     
     
class ALIGN_SUS_FOR_FULL_LOCK(GuardState):
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        for nodename in ['SUS_ETMY', 'SUS_ETMX', 'SUS_ITMY', 'SUS_ITMX', 'SUS_PRM', 'SUS_SRM']:
            if not nodes[nodename].NOTIFICATION:
                nodes[nodename] = 'ALIGNED'
            else:
                notify('NOt Aligning %s because of notification'%nodename)
        self.timer['wait_for_sus_settle'] = 10
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if self.timer['wait_for_sus_settle']:
            return True   


####################################
# MICH Locking

class MICH_SET(GuardState):
    request = False

    @assert_mc_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        #align        
        nodes['SUS_ITMY'] = 'ALIGNED'
        nodes['SUS_ITMX'] = 'ALIGNED'
        nodes['SUS_PRM'] = 'MISALIGNED'
        nodes['SUS_SRM'] = 'MISALIGNED'
        #nodes['SUS_ETMX'] = 'MISALIGNED'
        nodes['SUS_ETMY'] = 'MISALIGNED'
        #ASAIR45Q -> MICH
        for jj in range(1, 29):
            ezca['LSC-PD_DOF_MTRX_SETTING_3_%d'%jj] = 0
		# [OLD SCHOOL]
        #ezca['LSC-PD_DOF_MTRX_SETTING_2_19'] = 1
		intrix_ASPDs['MICH', 'ASAIR_A45Q'] = 1
        ezca['LSC-PD_DOF_MTRX_TRAMP'] = 1
        ezca['LSC-PD_DOF_MTRX_LOAD_MATRIX'] = 1
        ezca['LSC-ASPD_DOF_MTRX_TRAMP'] = 1
        ezca['LSC-ASPD_DOF_MTRX_LOAD_MATRIX'] = 1
        # Configure ASAIR whitening and filter since this is different for full lock
        ezca.switch('LSC-ASAIR_A_RF45_I','FM10', 'OFF')
        ezca.switch('LSC-ASAIR_A_RF45_I','FM9', 'ON')
        ezca['LSC-ASAIR_A_RF45_I_GAIN'] = 1.0
        ezca.switch('LSC-ASAIR_A_RF45_Q','FM10', 'OFF')
        ezca.switch('LSC-ASAIR_A_RF45_Q','FM9', 'ON')
        ezca['LSC-ASAIR_A_RF45_Q_GAIN'] = 1.0
        # As of Feb,9th,2015, the gain was decreased from 18 dB to 6 dB.
		# in order to avoid electronics offset issue which is due to different gain settings
        ezca['LSC-ASAIR_A_RF45_WHITEN_GAIN'] = 6.0 
        ezca['LSC-ASAIR_A_RF45_WHITEN_FILTER_1'] = 1.0
        ezca['LSC-ASAIR_A_RF45_AWHITEN_SET1'] = 1

        # Remove triggering
        ezca['LSC-MICH_TRIG_THRESH_ON'] = -10
        ezca['LSC-TRIG_MTRX_3_2'] = 0


		# set camera exposure to 80000. Good for 10W ?
		ezca['VID-CAM18_EXP'] = 40000
        # Remove filter triggering
        ezca['LSC-MICH_MASK_FM1'] = 0
        ezca['LSC-MICH_MASK_FM2'] = 0
        ezca['LSC-MICH_MASK_FM3'] = 0
        mich_filt().only_on( 'FM7', 'FM9', 'OUTPUT', 'DECIMATION')
        # timer that only gets set for non zero time if the suspensions really have to move
        self.timer['wait_for_sus_settle'] = 0 

    @assert_mc_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not nodes.arrived:
            self.timer['wait_for_sus_settle'] = 10
            notify('SUS Guardians working')
            return True
        else:
            if self.timer['wait_for_sus_settle']:
                notify('waiting for sus to settle')
                return True
        
class LOCK_MICH_DARK(GuardState):
    request = False
    @assert_mc_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        mich_filt().switch_on('INPUT')
        ezca['SUS-BS_M3_ISCINF_L_LIMIT'] = 1000000
        ezca.switch('SUS-BS_M3_ISCINF_L', 'LIMIT', 'ON')
        mich_filt().ramp_gain(lscparams.michdark_gain_acq, ramp_time=2, wait=False)

    @assert_mc_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):  
        ready_for_locking_MICH()
        if MICHDARK_locked():
            mich_filt().switch_on('FM2', 'FM3')
            mich_filt().ramp_gain(lscparams.michdark_gain, ramp_time=2, wait=False)
            ezca.switch('SUS-BS_M3_ISCINF_L', 'LIMIT', 'OFF')
            return True

class MICH_DARK_LOCKED(GuardState):
    request = True  
    @assert_mc_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker() 
    def run(self):
        if not MICHDARK_locked():
            mich_filt().switch_off('FM2', 'FM3')
            mich_filt().ramp_gain(lscparams.michdark_gain_acq, ramp_time=2, wait=False)
            ezca.switch('SUS-BS_M3_ISCINF_L', 'LIMIT', 'OFF')
            return 'LOCK_MICH_DARK'

#TODO: MICH states: MICH_GRAY


####################################
# PRXY Locking

def gen_SET_PRXY_state(arm):
    class SET_PRXY(GuardState):
        request = False

        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            if arm == 'X':
                nodes['SUS_ITMY'] = 'MISALIGNED'
                nodes['SUS_ITMX'] = 'ALIGNED'
            elif arm == 'Y':
                nodes['SUS_ITMX'] = 'MISALIGNED'
                nodes['SUS_ITMY'] = 'ALIGNED'
            nodes['SUS_ETMX'] = 'MISALIGNED'
            nodes['SUS_ETMY'] = 'MISALIGNED'
            nodes['SUS_PRM'] = 'ALIGNED'
            nodes['SUS_SRM'] = 'MISALIGNED'	
            #check input matrix (using REFLA RF 45 I)
            for jj in range(1, 29):
                ezca['LSC-PD_DOF_MTRX_SETTING_4_%d'%jj] = 0
            #this was when REFL was really refl
			# [OLD SCHOOL]
            #ezca['LSC-PD_DOF_MTRX_SETTING_3_6'] = 1
			intrix['PRCL', 'REFL_A9I'] = 1
            #Jan 31st REFL and reflair cables were swapped, swapped back Feb 2 2015
            #ezca['LSC-PD_DOF_MTRX_SETTING_3_16'] = 1
            ezca['LSC-PD_DOF_MTRX_TRAMP'] = 0
            time.sleep(0.1)
            ezca['LSC-PD_DOF_MTRX_LOAD_MATRIX'] = 1

           # Remove triggering
            ezca['LSC-TRIG_MTRX_3_2'] = 0
            ezca['LSC-PRCL_TRIG_THRESH_ON'] = -1e4

            # Remove filter triggering
            ezca['LSC-PRCL_MASK_FM2'] = 0
            ezca['LSC-PRCL_MASK_FM3'] = 0

            # Lock PRXY, Turn on locking filters
	        prcl_filt().only_on('DECIMATION', 'OUTPUT')
            for jj in range(0, len(lscparams.prcl_acquire_FMs)):
                prcl_filt().switch_on(lscparams.prcl_acquire_FMs[jj])
	        prm_m2_filt().only_on('DECIMATION', 'OUTPUT')
            for jj in range(0, len(lscparams.prm_m2_FMs)):
                prm_m2_filt().switch_on(lscparams.prm_m2_FMs[jj])
            ezca['LSC-PRCL_RSET'] = 2
            ezca['SUS-PRM_M2_LOCK_L_RSET'] = 2
            ezca['SUS-PRM_M2_LOCK_OUTSW_L'] = 'ON'
			ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L').switch_on('INPUT')
			ezca['SUS-PRM_M2_LOCK_L_GAIN'] = 0
			ezca['SUS-PRM_M2_LOCK_L_TRAMP'] = 5
		# timer that only gets set for non zero time if the suspensions really have to move
            self.timer['wait_for_sus_settle'] = 0 

        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if not nodes.arrived:
                self.timer['wait_for_sus_settle'] = 10
                notify('SUS Guardians working')
            else:
                if self.timer['wait_for_sus_settle']:
                    notify('waiting for sus to settle')
                    return True

    return SET_PRXY

def gen_LOCKING_PRXY_state(arm):
    class LOCKING_PRXY(GuardState):
        request = False
        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            # Ramp the gain
            prcl_filt().turn_on('INPUT')
            ezca['LSC-PRCL_TRAMP'] = 2
            ezca['LSC-PRCL_GAIN'] = lscparams.prxy_gain
            ezca['LSC-REFL_A_RF9_WHITEN_GAINSTEP'] = lscparams.prxy_REFL9_whiten_gain
        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
   	        ready_for_locking()
            if PRXY_oscillating():
                return 'DOWN'
            if PRXY_locked():
            	ezca['SUS-PRM_M2_LOCK_L_GAIN'] = lscparams.prm_M2_cross_over
                return True
    return LOCKING_PRXY

def gen_PRXY_LOCKED_state(arm):
    class PRXY_LOCKED(GuardState):
        request = True
        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if not PRXY_locked():
                return 'LOCKING_PR'+arm
            return True

    return PRXY_LOCKED

SET_PRX = gen_SET_PRXY_state('X')
LOCKING_PRX = gen_LOCKING_PRXY_state('X')
PRX_LOCKED = gen_PRXY_LOCKED_state('X')

'''SET_PRY = gen_SET_PRXY_state('Y')
LOCKING_PRY = gen_LOCKING_PRXY_state('Y')
PRY_LOCKED = gen_PRXY_LOCKED_state('Y')'''

####################################
# SRXY Locking

def gen_SET_SRXY_state(arm):
    class SET_SRXY(GuardState):
        request = False

        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            if arm == 'X':
                nodes['SUS_ITMY'] = 'MISALIGNED'
                nodes['SUS_ITMX'] = 'ALIGNED'
            elif arm == 'Y':
                nodes['SUS_ITMX'] = 'MISALIGNED'
                nodes['SUS_ITMY'] = 'ALIGNED'
            nodes['SUS_PRM'] = 'MISALIGNED'
            nodes['SUS_SRM'] = 'ALIGNED'	
            nodes['SUS_ETMX'] = 'MISALIGNED'
            nodes['SUS_ETMY'] = 'MISALIGNED'
            #check input matrix (using ASAIR 45 Q)
            for jj in range(1, 29):
                log ('setting matrix to 0')
                ezca['LSC-PD_DOF_MTRX_SETTING_5_%d'%jj] = 0

			# [OLD SCHOOL]
            #ezca['LSC-PD_DOF_MTRX_SETTING_4_19'] = 1
			intrix_ASPDs['SRCL', 'ASAIR_A45Q'] = 1
            ezca['LSC-PD_DOF_MTRX_TRAMP'] = 0
            ezca['LSC-ASPD_DOF_MTRX_TRAMP'] = 0
            ezca['LSC-PD_DOF_MTRX_LOAD_MATRIX'] = 1
            ezca['LSC-ASPD_DOF_MTRX_LOAD_MATRIX'] = 1

           # Remove triggering
            ezca['LSC-SRCL_TRIG_THRESH_ON'] = -1e4

            # Remove filter triggering
            ezca['LSC-SRCL_MASK_FM2'] = 0
            ezca['LSC-SRCL_MASK_FM3'] = 0

            # Lock PRXY, Turn on locking filters
	        srcl_filt().only_on('DECIMATION', 'OUTPUT')
            for jj in range(0, len(lscparams.srcl_acquire_FMs)):
                srcl_filt().switch_on(lscparams.srcl_acquire_FMs[jj])
	        srm_m2_filt().only_on('DECIMATION', 'OUTPUT')
            for jj in range(0, len(lscparams.srm_m2_FMs)):
                srm_m2_filt().switch_on(lscparams.srm_m2_FMs[jj])
            ezca['LSC-SRCL_RSET'] = 2
            # timer that only gets set for non zero time if the suspensions really have to move
            self.timer['wait_for_sus_settle'] = 0 

        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):

            if not nodes.arrived:
                self.timer['wait_for_sus_settle'] = 10
                notify('SUS Guardians working')
            else:
                if self.timer['wait_for_sus_settle']:
                    notify('waiting for sus to settle')
                    return True

    return SET_SRXY

def gen_LOCKING_SRXY_state(arm):
    class LOCKING_SRXY(GuardState):
        request = False
        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            # Ramp the gain
            srcl_filt().turn_on('INPUT')
            ezca['LSC-SRCL_TRAMP'] = 2
            ezca['LSC-SRCL_GAIN'] = lscparams.srxy_gain
        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
   	        ready_for_locking()
            #if SRXY_oscillating():
            #    return 'DOWN'
            if SRXY_locked():
                ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L').switch_on('INPUT')
                ezca['SUS-SRM_M2_LOCK_L_TRAMP'] = 5
                time.sleep(0.1)
                ezca['SUS-SRM_M2_LOCK_L_GAIN'] = 1               
                return True
    return LOCKING_SRXY

def gen_SRXY_LOCKED_state(arm):
    class SRXY_LOCKED(GuardState):
        request = True
        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if not SRXY_locked():
                return 'LOCKING_SR'+arm
            return True

    return SRXY_LOCKED

'''SET_SRX = gen_SET_SRXY_state('X')
LOCKING_SRX = gen_LOCKING_SRXY_state('X')
SRX_LOCKED = gen_SRXY_LOCKED_state('X')'''

SET_SRY = gen_SET_SRXY_state('Y')
LOCKING_SRY = gen_LOCKING_SRXY_state('Y')
SRY_LOCKED = gen_SRXY_LOCKED_state('Y')

####################################
# X/Y arm IR locking Locking

def gen_SET_ARM_IR_state(arm):
    class SET_ARM_IR(GuardState):
        request = False

        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            if arm == 'X':
                nodes['SUS_ITMY'] = 'MISALIGNED'
                nodes['SUS_ETMY'] = 'MISALIGNED'
                nodes['SUS_ITMX'] = 'ALIGNED'
                nodes['SUS_ETMX'] = 'ALIGNED'
            elif arm == 'Y':
                nodes['SUS_ITMY'] = 'ALIGNED'
                nodes['SUS_ETMY'] = 'ALIGNED'
                nodes['SUS_ITMX'] = 'MISALIGNED'
                nodes['SUS_ETMX'] = 'MISALIGNED'
            nodes['SUS_PRM'] = 'MISALIGNED'
            nodes['SUS_SRM'] = 'MISALIGNED'	
            # disable COMM slow feedback to IMC VCO
            ezca['ALS-C_COMM_PLLSLOW_ENABLE'] = 0

            #set input and output matrix (using ASAIR 45 I, sending signal to MC2)
            for jj in range(1, 29):
                log ('setting matrix to 0')
                ezca['LSC-PD_DOF_MTRX_SETTING_6_%d'%jj] = 0
                ezca['LSC-PD_DOF_MTRX_SETTING_7_%d'%jj] = 0
            for jj in range(1, 12):
                ezca['LSC-OUTPUT_MTRX_10_%d'%jj] = 0
            for jj in range(1, 7):
                ezca['LSC-OUTPUT_MTRX_%d_5'%jj] = 0
                ezca['LSC-OUTPUT_MTRX_%d_6'%jj] = 0
            if arm == 'X':
				#[OLD SCHOOL]
                #ezca['LSC-OUTPUT_MTRX_10_6'] = 1
                #ezca['LSC-PD_DOF_MTRX_SETTING_6_18'] = 1
				outrix['MC2', 'XARM'] = 1
				intrix_ASPDs['XARM', 'ASAIR_A45I'] = 1
            elif arm == 'Y':
				# [OLD SCHOOL]
                #ezca['LSC-OUTPUT_MTRX_10_7'] = 1
                #ezca['LSC-PD_DOF_MTRX_SETTING_7_18'] = 1         
				outrix['MC2', 'YARM'] = 1
				intrix_ASPDs['YARM', 'ASAIR_A45I'] = 1
            ezca['LSC-PD_DOF_MTRX_TRAMP'] = 0
            ezca['LSC-ASPD_DOF_MTRX_TRAMP'] = 0
            ezca['LSC-PD_DOF_MTRX_LOAD_MATRIX'] = 1
            ezca['LSC-ASPD_DOF_MTRX_LOAD_MATRIX'] = 1

           # Set Triggering
            ezca['LSC-TRIG_MTRX_6_18'] = 1
            ezca['LSC-TRIG_MTRX_7_19'] = 1
            ezca['LSC-%sARM_TRIG_THRESH_ON'%arm] = lscparams.arm_IR_trig_on
            ezca['LSC-%sARM_TRIG_THRESH_OFF'%arm] = lscparams.arm_IR_trig_off
            ezca['LSC-%sARM_FM_TRIG_THRESH_ON'%arm] = lscparams.arm_IR_FM_trig_on
            ezca['LSC-%sARM_FM_TRIG_THRESH_OFF'%arm] = lscparams.arm_IR_FM_trig_off

            # Remove filter triggering
            ezca['LSC-%sARM_MASK_FM1'%arm] = 1
            ezca['LSC-%sARM_MASK_FM2'%arm] = 1

            # Lock ARM IR, Turn on locking filters
	        arm_filt(arm).only_on('DECIMATION', 'OUTPUT')
            for jj in range(0, len(lscparams.arm_IR_acquire_FMs)):
                arm_filt(arm).switch_on(lscparams.arm_IR_acquire_FMs[jj])
	        #trusting IMC to be set correctly already
            ezca['LSC-%sARM_RSET'%arm] = 2
            # timer that only gets set for non zero time if the suspensions really have to move
            self.timer['wait_for_sus_settle'] = 0 

        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if not nodes.arrived:
                self.timer['wait_for_sus_settle'] = 10
                notify('SUS Guardians working')
            else:
                if not self.timer['wait_for_sus_settle']:
                    notify('waiting for sus to settle')
                else:
                    return True

    return SET_ARM_IR

def gen_LOCKING_ARM_IR_state(arm):
    class LOCKING_ARM_IR(GuardState):
        request = False
        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            # Ramp the gain
            ezca['LSC-%sARM_GAIN'%arm] = lscparams.arm_IR_gain[arm]
            arm_filt(arm).turn_on('INPUT')
            ezca['LSC-ASAIR_A_RF45_WHITEN_GAINSTEP'] = lscparams.arm_ASAIR_whiten_gain
            # make sure that 160 dBattenuator is off
            ezca.switch('LSC-ASAIR_A_RF45_I', 'FM10', 'OFF')
            ezca.switch('LSC-ASAIR_A_RF45_Q', 'FM10', 'OFF')
            self.timer['pause'] = 3
        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            ready_for_locking()
            if not ARM_IR_locked(arm): 
                self.timer['pause'] = 3
            #wait 3 seconds after boosts trigger to turn of MC gain
            elif self.timer['pause']: 
                ezca['IMC-MCL_TRAMP'] = 2      
                ezca['IMC-MCL_GAIN'] = 0      
                return True
    return LOCKING_ARM_IR

def gen_ARM_IR_LOCKED_state(arm):
    class ARM_IR_LOCKED(GuardState):
        request = True
        @assert_mc_locked
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            if not ARM_IR_locked(arm):
                return 'DOWN'
            return True

    return ARM_IR_LOCKED

SET_XARM_IR = gen_SET_ARM_IR_state('X')
LOCKING_XARM_IR = gen_LOCKING_ARM_IR_state('X')
XARM_IR_LOCKED = gen_ARM_IR_LOCKED_state('X')

SET_YARM_IR = gen_SET_ARM_IR_state('Y')
LOCKING_YARM_IR = gen_LOCKING_ARM_IR_state('Y')
YARM_IR_LOCKED = gen_ARM_IR_LOCKED_state('Y')

##################################################

edges = [
    #edges used just for suspension management
    ('DOWN', 'ALIGN_SUS_FOR_ALS'),
    ('DOWN', 'ALIGN_SUS_FOR_FULL_LOCK'),
    ('ALIGN_SUS_FOR_ALS', 'ALIGN_SUS_FOR_FULL_LOCK'),
# MICH edges:
    ('DOWN', 'MICH_SET'),
#    ('ALIGN_MICH','MICH_SET'),
    ('MICH_SET', 'LOCK_MICH_DARK'),
    ('LOCK_MICH_DARK', 'MICH_DARK_LOCKED'),
# PRX edges:
    ('DOWN','SET_PRX'),
    ('SET_PRX', 'LOCKING_PRX'),
    ('LOCKING_PRX', 'PRX_LOCKED'),
# SRY edges:
    ('DOWN', 'SET_SRY'),
    ('SET_SRY', 'LOCKING_SRY'),
    ('LOCKING_SRY', 'SRY_LOCKED'),
#XARM IR edges:
    ('DOWN','SET_XARM_IR'),
    ('SET_XARM_IR', 'LOCKING_XARM_IR'),
    ('LOCKING_XARM_IR', 'XARM_IR_LOCKED'),
#Y ARM IR edges:
    ('DOWN', 'SET_YARM_IR'),
    ('SET_YARM_IR', 'LOCKING_YARM_IR'),
    ('LOCKING_YARM_IR', 'YARM_IR_LOCKED')
    ]

##################################################
# SVN $Id$
# $HeadURL$
##################################################H1:LSC-ASAIR_A_RF45_WHITEN_GAIN
