# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: ISC_GEN_STATES.py 11154 2015-08-02 12:26:12Z evan.hall@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_GEN_STATES.py $

#import multiprocess as mpc SHeila comme ted this out since it caused an error
import subprocess

from guardian import GuardState, GuardStateDecorator
from ISC_library import *


##################################################
# Take care of WFS centering
##################################################
    
        
def gen_REFL_WFS_CENTERING(dof):
    class REFL_WFS_CENTERING(GuardState):
        request = False
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def main(self):
            # input matrix
            #clear_asc_input_matrix()
            #ezca['ASC-INMATRIX_P_12_17'] = 1
            #ezca['ASC-INMATRIX_Y_12_17'] = 1
            #ezca['ASC-INMATRIX_P_13_18'] = 1
            #ezca['ASC-INMATRIX_Y_13_18'] = 1
            #ezca['ASC-DC1_P_GAIN'] = -100
            #ezca['ASC-DC1_Y_GAIN'] = -50
            #ezca['ASC-DC2_P_GAIN'] = -300
            #ezca['ASC-DC2_Y_GAIN'] = -300
            if not ASC_DC_centering_servos_OK():
                notify('REFL WFS DC centering')
                ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                ezca['ASC-DC1_P_RSET'] = 2
                ezca['ASC-DC1_Y_RSET'] = 2
                ezca['ASC-DC2_P_RSET'] = 2
                ezca['ASC-DC2_Y_RSET'] = 2
            ezca.get_LIGOFilter('ASC-DC1_P').only_on('INPUT', 'FM1', 'FM2', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('ASC-DC1_Y').only_on('INPUT', 'FM1', 'FM2', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('ASC-DC2_P').only_on('INPUT', 'FM1', 'FM2', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('ASC-DC2_Y').only_on('INPUT', 'FM1', 'FM2', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
            self.timer['step'] = 1

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            if not ASC_DC_centering_servos_OK():
                notify('REFL WFS DC centering railed')
                ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')                
                ezca['ASC-DC1_P_RSET'] = 2
                ezca['ASC-DC1_Y_RSET'] = 2
                ezca['ASC-DC2_P_RSET'] = 2
                ezca['ASC-DC2_Y_RSET'] = 2
            # define thresholds
            reflwfs_threshold_dc = 0.5 # threshold on the WFS centering
            # wait for centering to reach a reasonable value
            err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON'))
            log(max(abs(err)))
            #log(err)
            if max(abs(err)) > reflwfs_threshold_dc:
                if self.timer['step']:
                    err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON'))
                    self.timer['step'] = 1
            else:
                return True
    return REFL_WFS_CENTERING

def gen_ENGAGE_CORNER_WFS_CENTERING(dof):
    class ENGAGE_CORNER_WFS_CENTERING(GuardState):
            request = False
            @assert_mc_locked
            @assert_dof_locked_gen(dof)
            #@get_subordinate_watchdog_check_decorator(self.nodes)
            #@nodes.checker()
            def main(self):
                if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                    notify('Toast is ready!')
                    ezca['ISI-HAM6_WD_RSET'] = 1
                    time.sleep(1)
                    ezca['ISI-HAM6_DACKILL_RESET'] = 1
                    ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter  
                # input matrix
                log('turning on DC centering')
                #ezca['ASC-INMATRIX_P_14_19'] = 1  # AS_A --> DC3 --> OM2
                #ezca['ASC-INMATRIX_Y_14_19'] = 1
                ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')
                #turn on OMC QPD loops
                ezca['OMC-ASC_MASTERGAIN'] = 0
                ezca['OMC-ASC_QDSLIDER'] = 1.0  # I don't like this name, also 1.0 is at the bottom of the slider.

                # Turn on the OMC ASC gain gently, the compensation filters for the OMC SUS have some transient response
                cdsutils.step(ezca, 'OMC-ASC_MASTERGAIN','0.1,2',0.2)
                #temporarily commented out , SED March 30th because we were having truoble with AS_B being miscentered.
                # Don't use the boosts (FM1), it's too much gain
                # Integrators (FM2) should always be on
                #set_asc_boosts('ON')
                omc_asc_integrators('ON')
                self.timer['wait'] = 1

                if not CORNER_WFS_DC_centering_servos_OK():  
                    notify('REFL or AS WFS DC centering')
                    ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')
                    ezca['ASC-DC1_P_RSET'] = 2
                    ezca['ASC-DC1_Y_RSET'] = 2
                    ezca['ASC-DC2_P_RSET'] = 2
                    ezca['ASC-DC2_Y_RSET'] = 2
                    ezca['ASC-DC3_P_RSET'] = 2
                    ezca['ASC-DC3_Y_RSET'] = 2
                    ezca['ASC-DC4_P_RSET'] = 2
                    ezca['ASC-DC4_Y_RSET'] = 2

                    self.timer['wait'] = 1
                    ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')



            def run(self):
                #if self.timer['wait']:
                #    ezca.get_LIGOFilter('ASC-DC1_P').switch_on('FM1')
                #    ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('FM1')
                #    ezca.get_LIGOFilter('ASC-DC2_P').switch_on('FM1')
                #    ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('FM1')
                return True
                    
    return ENGAGE_CORNER_WFS_CENTERING


def gen_CORNER_WFS_CENTERING(dof):
    class CORNER_WFS_CENTERING(GuardState):
        request = False
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def main(self):
            self.timer['step'] = 1

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def run(self):
            if not CORNER_WFS_DC_centering_servos_OK():
                notify('REFL or AS WFS DC centering railed')
                ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')
                ezca['ASC-DC1_P_RSET'] = 2
                ezca['ASC-DC1_Y_RSET'] = 2
                ezca['ASC-DC2_P_RSET'] = 2
                ezca['ASC-DC2_Y_RSET'] = 2
                ezca['ASC-DC3_P_RSET'] = 2
                ezca['ASC-DC3_Y_RSET'] = 2
                ezca['ASC-DC4_P_RSET'] = 2
                ezca['ASC-DC4_Y_RSET'] = 2
                time.sleep(1)
                ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')
            # define thresholds
            threshold_dc = 0.2 # threshold on the WFS centering
            # wait for centering to reach a reasonable value
            err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON', 'ASC-DC3_P_INMON', 'ASC-DC3_Y_INMON'))  # removed DC4
            if max(abs(err)) > threshold_dc:
                if self.timer['step']:
                    if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                        notify('Toast is ready!')
                        ezca['ISI-HAM6_WD_RSET'] = 1
                        time.sleep(1)
                        ezca['ISI-HAM6_DACKILL_RESET'] = 1
                        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter
                    err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON', 'ASC-DC3_P_INMON', 'ASC-DC3_Y_INMON'))  # removed DC4
                    self.timer['step'] = 1
            else:
                return True
    return CORNER_WFS_CENTERING

def gen_OFFLOAD_ALIGNMENT(dof, ramptime):
    class OFFLOAD_ALIGNMENT(GuardState):
        request = True
        redirect = False
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def main(self):

            old_gain_p = [None]*len(self.optics)
            old_gain_y = [None]*len(self.optics)

            #offload
            for i in range(len(self.optics)):
                log('starting smooth offload')
                smooth_offload(self.optics[i], ramptime) # changed from offload SED and CCW March 13 2015

        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            return True
    return OFFLOAD_ALIGNMENT

def gen_OFFLOAD_ALIGNMENT_MANY(dof, ramptime):
    class OFFLOAD_ALIGNMENT(GuardState):
        request = True
        redirect = False
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def main(self):

            old_gain_p = [None]*len(self.optics)
            old_gain_y = [None]*len(self.optics)

            #offload
            log('starting smooth offload')
            smooth_offload_fast(self.optics, ramptime)
            #smooth_offload_many(self.optics, ramptime)

        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            return True
    return OFFLOAD_ALIGNMENT

def gen_OFFLOAD_ALIGNMENT_SIMULT(dof, ramptime):
    # =====> UNTESTED
    class OFFLOAD_ALIGNMENT(GuardState):
        request = True
        redirect = False
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def main(self):

            old_gain_p = [None]*len(self.optics)
            old_gain_y = [None]*len(self.optics)

            #offload
            for i in range(len(self.optics)):
                log('starting smooth offload')
                p = mpc.Process(target=smooth_offload, args=(self.optics[i], ramptime))
                p.start()

        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            return True
    return OFFLOAD_ALIGNMENT


