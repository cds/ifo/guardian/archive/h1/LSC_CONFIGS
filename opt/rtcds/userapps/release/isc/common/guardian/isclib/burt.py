from subprocess import call

from guardian.ligopath import userapps_path

def iscBurt(burtfile):
    # build the burt command
    ifo = IFO.lower()
    snap = userapps_path('isc', ifo, 'burtfiles', burtfile)
    log('BURT RESTORE: ' + snap)
    # run burtwb (this is blocking, but quick)
    call(['burtwb', '-f', snap])
